TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := mips

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/toolchains/mips-linux-musl-mips32/bin/mips-linux-musl-

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/generic/config/
